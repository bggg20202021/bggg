import json
import datetime


def levenshtein_uzakligi(kelime1, kelime2):
    matris = [[0 for i in range(len(kelime1) + 1)] for j in range(len(kelime2) + 1)]
    for i in range(len(kelime1) + 1):
        matris[0][i] = i

    for j in range(len(kelime2) + 1):
        matris[j][0] = j

    for i in range(1, len(kelime1) + 1):
        for j in range(1, len(kelime2) + 1):
            if kelime1[i - 1] == kelime2[j - 1]:
                matris[j][i] = min(matris[j][i - 1] + 10, matris[j - 1][i] + 10, matris[j - 1][i - 1])
            else:
                matris[j][i] = min(matris[j][i - 1] + 10, matris[j - 1][i] + 10, matris[j - 1][i - 1] + 1)

    return matris[len(kelime2)][len(kelime1)]


class HurriyetAramaMotoru:

    def __init__(self):
        self.ham_dokumanlar = []
        self.islenmis_dokumanlar = []
        self.inverted_index = []
        self.permuterm_index = []

    def yukle(self, dosyaadi):
        print("{} yükleniyor...".format(dosyaadi))
        dosya = open(dosyaadi, "r", encoding="utf8")
        veri = json.load(dosya)
        dosya.close()

        for haber in veri['List']:
            self.ham_dokumanlar.append(
                haber['Title'].strip() + " " + haber['Description'].strip() + " " + haber['Text'].strip())

        print("{} yüklendi, toplam {} adet döküman...".format(dosyaadi, len(self.ham_dokumanlar)))

    def onisleme(self):
        print("Önişleme başlıyor...")
        self.islenmis_dokumanlar = []

        alfabe = "abcçdefgğhıijklmnoöpqrsştuüvwxyz "
        alfabe_liste = list(alfabe)

        for dokuman in self.ham_dokumanlar:
            # Küçük harfe çevirelim...
            dokuman = dokuman.lower()

            # alfabede olmayan harfleri çıkartalım...
            dokuman_liste = list(dokuman)

            yeni_dokuman = []

            for harf in dokuman_liste:
                if harf in alfabe_liste:
                    yeni_dokuman.append(harf)

            yeni_dokuman_string = "".join(yeni_dokuman)

            # işlenmiş dokümanlara ekleyelim...
            self.islenmis_dokumanlar.append(yeni_dokuman_string)
        print("Önişleme tamamlandı...")

    def dizinle(self):
        print("Dizinleme başladı...")
        ilk_asama = []

        for docid, dokuman in enumerate(self.islenmis_dokumanlar):

            kelimeler = dokuman.split(" ")

            for kelime in kelimeler:

                if len(kelime) == 0:
                    continue

                eklenecek_tuple = (kelime, docid)

                ilk_asama.append(eklenecek_tuple)

        ilk_asama.sort()

        son_kelime = ""

        self.inverted_index = []

        for kelime, docid in ilk_asama:

            if kelime != son_kelime:
                son_kelime = kelime
                eklenecek_tuple = (kelime, 1, [docid])
                self.inverted_index.append(eklenecek_tuple)
            else:
                kelime, frekans, postings = self.inverted_index[-1]

                if docid not in postings:
                    postings.append(docid)
                    frekans += 1

                    yeni_tuple = (kelime, frekans, postings)

                    self.inverted_index[-1] = yeni_tuple

        self.permuterm_index = []

        for word, freq, posting in self.inverted_index:
            ozel_karakter = '$'

            k1 = word + ozel_karakter

            self.permuterm_index.append((k1, word))

            for i in range(len(word)):
                k1 = k1[-1] + k1[0:len(word)]
                self.permuterm_index.append((k1, word))
        print("Dizinleme tamamlandı...")

    def kelime_kumesi(self, word):
        for kelime, frekans, posting in self.inverted_index:
            if kelime == word:
                postingkumesi = set(posting)
                return postingkumesi

    def permuterm_kumesi(self, permuterm):
        if '*' in permuterm:
            if permuterm.endswith('*') and permuterm.startswith('*'):
                # Permuterm *X*
                X = permuterm[1:len(permuterm) - 1]
                return self.permutermleri_bul(X)
            elif permuterm.startswith('*'):
                # Permuterm *X
                X = permuterm[1:len(permuterm)] + "$"
                return self.permutermleri_bul(X)
            elif permuterm.endswith('*'):
                # Permuterm X*
                X = "$" + permuterm[0:len(permuterm) - 1]
                return self.permutermleri_bul(X)
            else:
                # Permuterm X*Y
                yildiz_konum = permuterm.find('*')
                X = permuterm[0:yildiz_konum]
                Y = permuterm[yildiz_konum + 1:len(permuterm)]
                T = Y + '$' + X
                return self.permutermleri_bul(T)
        else:
            # Permuterm X
            return self.kelime_kumesi(permuterm)

    def permutermleri_bul(self, X):
        posting_kumesi = set()
        for permuterm, word in self.permuterm_index:
            if permuterm.startswith(X):
                posting_kumesi = posting_kumesi.union(self.kelime_kumesi(word))
        return posting_kumesi

    def sorgu_calistir(self, sorgu_metni):
        print("Sorgu Çalıştırılıyor...")
        sorgu_kelimeleri = sorgu_metni.split(" ")

        sonuc_kumesi = self.permuterm_kumesi(sorgu_kelimeleri[0])

        for i in range(1, len(sorgu_kelimeleri)):
            gecici_kume = self.permuterm_kumesi(sorgu_kelimeleri[i])
            sonuc_kumesi = sonuc_kumesi.intersection(gecici_kume)

        sonuc = []

        for docid in sonuc_kumesi:
            sonuc.append(self.ham_dokumanlar[docid])

        return sonuc

    def did_you_mean(self, kelime):
        kelime_uzakliklari = []

        for word, freq, posting in self.inverted_index:
            uzaklik = levenshtein_uzakligi(word, kelime)
            uzaklik_bilgisi = (uzaklik, word)
            kelime_uzakliklari.append(uzaklik_bilgisi)

        kelime_uzakliklari.sort()

        return kelime_uzakliklari


def main():
    aramaMotoru = HurriyetAramaMotoru()

    aramaMotoru.yukle("veri/ekonomi_0.json")
    aramaMotoru.yukle("veri/ekonomi_1.json")
    aramaMotoru.yukle("veri/ekonomi_2.json")
    aramaMotoru.yukle("veri/saglik_0.json")
    aramaMotoru.yukle("veri/saglik_1.json")
    aramaMotoru.yukle("veri/saglik_2.json")
    aramaMotoru.onisleme()
    aramaMotoru.dizinle()

    baslangic = datetime.datetime.now()
    sonuclar = aramaMotoru.sorgu_calistir("ekonomi*")
    bitis = datetime.datetime.now()

    print("{} adet döküman {} sürede bulundu".format(len(sonuclar), bitis - baslangic))

    baslangic = datetime.datetime.now()
    didyoumean = aramaMotoru.did_you_mean("kasner")
    bitis = datetime.datetime.now()

    print("{} sürede bulundu".format(len(sonuclar), bitis - baslangic))

    for i in range(30):
        print(i, didyoumean[i][1], didyoumean[i][0])

    sira_no = 1

    for dokuman in sonuclar:
        print(sira_no, dokuman)
        sira_no += 1


if __name__ == '__main__':
    main()
