import json
import datetime

import numpy
import numpy as np
import scipy.sparse as sparse
from numpy import array
import os
import pickle

from sklearn import metrics
from sklearn.linear_model import RidgeClassifier, Perceptron, PassiveAggressiveClassifier
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from unicode_tr import unicode_tr
from snowballstemmer import TurkishStemmer

from multiprocessing import Pool

import logging
logging.basicConfig(format='%(asctime)s: %(levelname)s : %(message)s', level=logging.INFO)


def bir_dokuman_onisle(dokuman: str):
    # turkStem = TurkishStemmer()
    alfabe = "abcçdefgğhıijklmnoöpqrsştuüvwxyz "
    alfabe_liste = list(alfabe)
    gereksiz_kelimeler = ["quot", "fotoğraflı", "fotografli", "dedi", "daha", "veya", "yada", "dha", "ama",
                          "fakat", "ancak", "iyi", "foto", "şey", "nin", "bir", "çok",  "içi", "için", "gibi"]

    # Küçük harfe çevirelim...
    dokuman = unicode_tr(dokuman).lower()
    # alfabede olmayan harfleri çıkartalım...
    dokuman_liste = list(dokuman)
    yeni_dokuman = [harf if harf in alfabe_liste else " " for harf in dokuman_liste]

    yeni_dokuman_string = "".join(yeni_dokuman)
    yeni_dokuman = yeni_dokuman_string.split(" ")
    yeni_dokuman = [kelime for kelime in yeni_dokuman if len(kelime) > 0]

    # stemming (gövdeleme) eksik...
    # yeni_dokuman = turkStem.stemWords(yeni_dokuman)

    # stop word eliminasyonu eksik...
    yeni_dokuman = [kelime for kelime in yeni_dokuman if kelime not in gereksiz_kelimeler]

    # Kelime boyutu en az 3 karakter olmalı
    yeni_dokuman = [kelime for kelime in yeni_dokuman if len(kelime) >= 3]

    yeni_dokuman_string = " ".join(yeni_dokuman)

    return yeni_dokuman_string


def levenshtein_uzakligi(kelime1, kelime2):
    matris = [[0 for i in range(len(kelime1) + 1)] for j in range(len(kelime2) + 1)]
    for i in range(len(kelime1) + 1):
        matris[0][i] = i

    for j in range(len(kelime2) + 1):
        matris[j][0] = j

    for i in range(1, len(kelime1) + 1):
        for j in range(1, len(kelime2) + 1):
            if kelime1[i - 1] == kelime2[j - 1]:
                matris[j][i] = min(matris[j][i - 1] + 10, matris[j - 1][i] + 10, matris[j - 1][i - 1])
            else:
                matris[j][i] = min(matris[j][i - 1] + 10, matris[j - 1][i] + 10, matris[j - 1][i - 1] + 1)

    return matris[len(kelime2)][len(kelime1)]


class HurriyetAramaMotoru:

    def __init__(self):
        self.kategoriler = []
        self.ham_dokumanlar = []
        self.islenmis_dokumanlar = []
        self.sozluk = None
        self.terim_frekanslari = None
        self.tf = None
        self.idf = None
        self.norm_tfidf_mat = None

    def yukle(self, dosyaadi, kategori):
        print("{} yükleniyor...".format(dosyaadi))
        dosya = open(dosyaadi, "r", encoding="utf8")
        veri = json.load(dosya)
        dosya.close()

        for haber in veri['List']:
            self.ham_dokumanlar.append(
                haber['Title'].strip() + " " + haber['Description'].strip() + " " + haber['Text'].strip())
            self.kategoriler.append(kategori)

        print("{} yüklendi, toplam {} adet döküman...".format(dosyaadi, len(self.ham_dokumanlar)))

    def onisleme_paralel(self):
        if os.path.exists("onisleme_sonucu.pkl"):
            with open("onisleme_sonucu.pkl", "rb") as f:
                self.islenmis_dokumanlar = pickle.load(f)
            return

        print("Önişleme başlıyor...")

        pool = Pool(processes=7)

        gecici_sonuc = [pool.apply_async(bir_dokuman_onisle, args=(d,)) for d in self.ham_dokumanlar]

        for i, sonuc in enumerate(gecici_sonuc):
            self.islenmis_dokumanlar.append(sonuc.get())
            print(i, ". döküman tamamlandı!", end="\r", flush=True)

        print("\nÖnişleme tamamlandı...")
        with open("onisleme_sonucu.pkl", "wb") as f:
            pickle.dump(self.islenmis_dokumanlar, f)

    def onisleme(self):
        if os.path.exists("onisleme_sonucu.pkl"):
            with open("onisleme_sonucu.pkl", "rb") as f:
                self.islenmis_dokumanlar = pickle.load(f)
        else:
            turkStem = TurkishStemmer()

            print("Önişleme başlıyor...")
            self.islenmis_dokumanlar = []

            alfabe = "abcçdefgğhıijklmnoöpqrsştuüvwxyz "
            alfabe_liste = list(alfabe)

            gereksiz_kelimeler = ["quot", "fotoğraflı", "fotografli", "dedi", "daha", "veya", "yada", "dha", "ama",
                                  "fakat", "ancak"]

            for i, dokuman in enumerate(self.ham_dokumanlar):
                # Küçük harfe çevirelim...
                dokuman = unicode_tr(dokuman).lower()

                # alfabede olmayan harfleri çıkartalım...
                dokuman_liste = list(dokuman)

                yeni_dokuman = [harf if harf in alfabe_liste else " " for harf in dokuman_liste]

                # yeni_dokuman = []
                #
                # for harf in dokuman_liste:
                #    if harf in alfabe_liste:
                #        yeni_dokuman.append(harf)
                #    else:
                #        yeni_dokuman.append(" ")
                yeni_dokuman_string = "".join(yeni_dokuman)
                yeni_dokuman = yeni_dokuman_string.split(" ")
                yeni_dokuman = [kelime for kelime in yeni_dokuman if len(kelime) > 0]

                # stemming (gövdeleme) eksik...
                yeni_dokuman = turkStem.stemWords(yeni_dokuman)

                # stop word eliminasyonu eksik...
                yeni_dokuman = [kelime for kelime in yeni_dokuman if kelime not in gereksiz_kelimeler]

                # Kelime boyutu en az 3 karakter olmalı
                yeni_dokuman = [kelime for kelime in yeni_dokuman if len(kelime) >= 3]

                yeni_dokuman_string = " ".join(yeni_dokuman)

                # işlenmiş dokümanlara ekleyelim...
                self.islenmis_dokumanlar.append(yeni_dokuman_string)
                print(i, ". doküman işlendi!", end="\r", flush=True)
            print("\nÖnişleme tamamlandı...")
            with open("onisleme_sonucu.pkl", "wb") as f:
                pickle.dump(self.islenmis_dokumanlar, f)

    def sozluk_olustur(self):
        if os.path.exists("sozluk.pkl"):
            with open("sozluk.pkl", "rb") as f:
                self.sozluk = pickle.load(f)
            return
        gecici = set()

        for i, dokuman in enumerate(self.islenmis_dokumanlar):
            dokuman_kelimeleri = dokuman.split(" ")

            for kelime in dokuman_kelimeleri:
                if len(kelime) > 0:
                    gecici.add(kelime)

            print(i, ". doküman işlendi!", end="\r", flush=True)

        print("")

        # Sözlüğü numpy'ye çevirelim
        gecici_liste = list(gecici)
        gecici_liste.sort()

        self.sozluk = np.array(gecici_liste)
        with open("sozluk.pkl", "wb") as f:
            pickle.dump(self.sozluk, f)

    def dizinle(self):
        print("Dizinleme başladı...")

        print("Sözlük oluşturma işlemine başlandı...")
        self.sozluk_olustur()
        print("Sözlük oluşturma işlemi tamamlandı (KS:{})...".format(len(self.sozluk)))
        if os.path.exists("terim_frekanslari.pkl"):
            with open("terim_frekanslari.pkl", "rb") as f:
                self.terim_frekanslari = pickle.load(f)
            return

        gecici = sparse.lil_matrix((len(self.islenmis_dokumanlar), len(self.sozluk)), dtype=np.int32)

        for i, dokuman in enumerate(self.islenmis_dokumanlar):
            dokuman_kelimeleri = dokuman.split(" ")

            dokuman_kelimeleri = [kelime for kelime in dokuman_kelimeleri if len(kelime) > 0]

            # 111. satırın alternatifi
            # temp = []
            # for kelime in dokuman_kelimeleri:
            #     if len(kelime) > 0:
            #         temp.append(kelime)
            # dokuman_kelimeleri = temp
            # alternatif sonu

            benzersiz_kelimeler, frekanslar = np.unique(dokuman_kelimeleri, return_counts=True)

            kelimelerin_sozluk_konumlari = np.searchsorted(self.sozluk, benzersiz_kelimeler)

            gecici[i, kelimelerin_sozluk_konumlari] = frekanslar
            print(i, ". doküman işlendi!", end="\r", flush=True)

        print("")
        self.terim_frekanslari = gecici.tocsr()

        with open("terim_frekanslari.pkl", "wb") as f:
            pickle.dump(self.terim_frekanslari, f)

        print("Dizinleme tamamlandı...")

    def tfidf(self):
        # w = 1 + log(tf), eğer tf>=1
        # w = 0, aksi halde!

        if os.path.exists("tf.pkl"):
            with open("tf.pkl", "rb") as f:
                self.tf = pickle.load(f)
        else:
            print("TF Hesaplanıyor...")
            self.tf = self.terim_frekanslari.copy().astype(np.float64)
            maksimumlar = self.tf.max(axis=1).toarray().flatten()
            satirlardaki_eleman_sayisi = [self.tf[i, :].nnz for i in range(len(self.islenmis_dokumanlar))]
            maksimumlar = np.repeat(maksimumlar, satirlardaki_eleman_sayisi)
            self.tf.data = 0.5 + ((0.5 * self.tf.data)/maksimumlar)
            print("TF Hesaplandı...")
            with open("tf.pkl", "wb") as f:
                pickle.dump(self.tf, f)

        if os.path.exists("idf.pkl"):
            with open("idf.pkl", "rb") as f:
                self.idf = pickle.load(f)
                self.df = pickle.load(f)
        else:
            print("IDF Hesaplanıyor...")

            # idf = log(N/df)
            self.df = numpy.zeros((len(self.sozluk)), dtype=np.float64)

            N = len(self.sozluk)

            gecici = self.terim_frekanslari.tocsc()

            self.df = np.array([gecici[:, sutun].nnz for sutun in range(N)])

            # for sutun in range(N):
            #     self.df[sutun] = gecici[:, sutun].nnz
            #     print(sutun, ". kelime tamam", end="\r", flush=True)
            # print("")

            self.idf = np.log10(N / self.df)

            print("IDF Hesaplandı...")
            with open("idf.pkl", "wb") as f:
                pickle.dump(self.idf, f)
                pickle.dump(self.df, f)

        if os.path.exists("tfidf.pkl"):
            with open("tfidf.pkl", "rb") as f:
                self.tfidf_mat = pickle.load(f)
        else:
            print("TF-IDF Hesaplanıyor...")
            self.tfidf_mat = self.tf.copy().tocsc()

            weights = np.repeat(self.idf, self.df.astype(np.int32))

            self.tfidf_mat.data *= weights

            self.tfidf_mat = self.tfidf_mat.tocsr()
            print("TF-IDF Hesaplandı...")
            with open("tfidf.pkl", "wb") as f:
                pickle.dump(self.tfidf_mat, f)

    def tfidf_normalize(self):
        if os.path.exists("tfidf_norm.pkl"):
            with open("tfidf_norm.pkl", "rb") as f:
                self.norm_tfidf_mat = pickle.load(f)
            return
        print("TF-IDF Normalize Ediliyor...")
        t = self.tfidf_mat.copy()
        uzunluklar = np.sqrt(np.array(t.multiply(t).sum(axis=1)).flatten())
        uzunluk_repeats = np.array([t[i, :].nnz for i in range(len(self.islenmis_dokumanlar))])
        uzunluklar = np.repeat(uzunluklar, uzunluk_repeats)

        t.data /= uzunluklar

        self.norm_tfidf_mat = t

        print("TF-IDF Normalize edildi")

        with open("tfidf_norm.pkl", "wb") as f:
            pickle.dump(self.norm_tfidf_mat, f)

    def sorgu_calistir(self, sorgu_metni):
        print("Sorgu Çalıştırılıyor...")

        # sorgu önişlemeden geçmeli!!!

        # Sorgu vektörü için TF-IDF hesaplaması başlangıcı
        kelimeler = sorgu_metni.split(" ")

        kelimelerin_sozluk_konumlari = np.searchsorted(self.sozluk, kelimeler)

        sorgu_vektoru = np.zeros((len(self.sozluk)), dtype=np.float64)

        sorgu_vektoru[kelimelerin_sozluk_konumlari] = 1.0

        uzunluk = np.sqrt(np.sum(sorgu_vektoru * sorgu_vektoru))

        sorgu_vektoru = sorgu_vektoru / uzunluk
        # Sorgu vektörü için TF-IDF hesaplaması sonu

        sonuc_vektoru = self.norm_tfidf_mat * sorgu_vektoru

        dokumanlar = np.flip(np.argsort(sonuc_vektoru))

        sonuc_vektoru = np.flip(np.sort(sonuc_vektoru))

        return dokumanlar, sonuc_vektoru

    def basarim(self, siniflandirici, X_e, Y_e, X_t, Y_t, kategoriler):
        print('_' * 80)
        print("Eğitim Başladı: ")
        print(siniflandirici)
        t0 = datetime.datetime.now()
        siniflandirici.fit(X_e, Y_e)
        train_time = datetime.datetime.now() - t0
        print("Eğitim Süresi: {}".format(train_time))

        t0 = datetime.datetime.now()
        pred = siniflandirici.predict(X_t)
        test_time = datetime.datetime.now() - t0
        print("Test Süresi: {}".format(test_time))

        score = metrics.f1_score(Y_t, pred, average='micro')
        print("F1-Skoru:   {}".format(score))
        print()

        if hasattr(siniflandirici, 'coef_'):
            for i, kat in enumerate(kategoriler):
                enonemli10 = np.flip(np.argsort(siniflandirici.coef_[i])[-10:])
                print("{}: {}".format(kat, " ".join(self.sozluk[enonemli10])))
            print()

        print(metrics.classification_report(Y_t, pred, target_names=kategoriler))

        print("Karışıklık Matrisi:")
        print(metrics.confusion_matrix(Y_t, pred))

        print()

    def konuAnalizi(self):
        # Topic Analyze
        from gensim import corpora
        from collections import defaultdict
        from gensim import models

        dokumanlar = self.islenmis_dokumanlar[:2000]

        tokenlar = [[kelime for kelime in dokuman.split()] for dokuman in dokumanlar]

        frekanslar = defaultdict(int)

        for dokuman_tokenlari in tokenlar:
            for token in dokuman_tokenlari:
                frekanslar[token] += 1

        tokenlar = [[token for token in dokuman_tokenlari if frekanslar[token]>1] for dokuman_tokenlari in tokenlar]

        sozluk = corpora.Dictionary(tokenlar)

        gecici = sozluk[0]

        dokuman_koleksiyonu = [sozluk.doc2bow(dokuman_tokenlari) for dokuman_tokenlari in tokenlar]

        konu_sayisi = 20
        bellek_miktari = 1000
        uzerinden_gecme = 20
        dongu_sayisi = 400
        degerlendirme_peryodu = 100

        model = models.LdaModel(
            corpus=dokuman_koleksiyonu,
            id2word=sozluk.id2token,
            chunksize=bellek_miktari,
            alpha='auto',
            eta='auto',
            iterations=dongu_sayisi,
            num_topics=konu_sayisi,
            passes=uzerinden_gecme,
            eval_every=degerlendirme_peryodu
        )

        print(model)


    def siniflandir(self, test_ratio=0.2, use_raw=True):
        np_kat = np.array(self.kategoriler)

        unq_kat, frekans = np.unique(np_kat, return_counts=True)

        print(unq_kat, frekans)

        Y = np.zeros_like(np_kat, dtype=np.uint8)

        for i, kat in enumerate(unq_kat):
            Y[np_kat == kat] = i

        X = self.tfidf_mat
        if not use_raw:
            X = self.norm_tfidf_mat

        X_e, X_t, Y_e, Y_t = train_test_split(X, Y, test_size=test_ratio)

        print("Eğitim ve test kümeleri oluşturuldu!")

        for siniflandirici, isim in (
                (Perceptron(), "YSA Sınıflandırıcısı"),
                (PassiveAggressiveClassifier(), "Pasif-Agresif YSA Sınıflandırıcısı"),
                # (RidgeClassifier(tol=1e-2, solver="auto", max_iter=15), "Ridge Sınıflandırma"),
                (KNeighborsClassifier(n_neighbors=10), "kEn Yakın Komşu Sınıflandırıcısı"),
                (MultinomialNB(alpha=.01), "Multinomial Naive Bayes"),
                (BernoulliNB(alpha=.01), "Multinomial Naive Bayes")):
            print('=' * 80)
            print(isim)
            self.basarim(siniflandirici, X_e, Y_e, X_t, Y_t, unq_kat)


if __name__ == '__main__':
    aramaMotoru = HurriyetAramaMotoru()

    dosyalar = sorted(os.listdir("veri"))

    for dosya in dosyalar:
        ind = dosya.find('_')
        kategori = dosya[:ind]
        aramaMotoru.yukle("veri/{}".format(dosya), kategori)

    aramaMotoru.onisleme_paralel()

    aramaMotoru.dizinle()

    aramaMotoru.tfidf()

    aramaMotoru.tfidf_normalize()

    doc_ids, skor = aramaMotoru.sorgu_calistir("seçim chp")

    for i in range(len(doc_ids)):
        if i > 100:
            break
        if skor[i] <= 0.0:
            break
        print(i, ":", aramaMotoru.ham_dokumanlar[doc_ids[i]][:75], skor[i])

    # aramaMotoru.siniflandir()

    aramaMotoru.konuAnalizi()


